package com.cuba.hardcoding.chat.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Message")
public class Message {
    @DatabaseField(generatedId = true)
    protected int id;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    protected String text_message;
    @DatabaseField(canBeNull = false, dataType = DataType.DATE_TIME)
    protected Date date_time;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    protected String state;

    @DatabaseField(canBeNull = true, foreign = true)
    private User user_emisor;
    @DatabaseField(canBeNull = true, foreign = true)
    private User user_receiver;

    public Message() {
    }

    public Message(String text_message, Date date_time, String state) {
        this.text_message = text_message;
        this.date_time = date_time;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText_message() {
        return text_message;
    }

    public void setText_message(String text_message) {
        this.text_message = text_message;
    }

    public Date getDate_time() {
        return date_time;
    }

    public void setDate_time(Date date_time) {
        this.date_time = date_time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public User getUser_emisor() {
        return user_emisor;
    }

    public void setUser_emisor(User user_emisor) {
        this.user_emisor = user_emisor;
    }

    public User getUser_receiver() {
        return user_receiver;
    }

    public void setUser_receiver(User user_receiver) {
        this.user_receiver = user_receiver;
    }
}

