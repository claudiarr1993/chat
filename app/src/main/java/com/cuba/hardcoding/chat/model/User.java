package com.cuba.hardcoding.chat.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;


@DatabaseTable(tableName = "User")
public class User {
    @DatabaseField(generatedId = true)
    protected int id;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, uniqueIndex = true)
    protected String email;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, index = true)
    protected String name;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, index = true)
    protected String lastName;
    @DatabaseField(canBeNull = true, dataType = DataType.DATE_STRING) //set to false
    protected Date dateOfBirth;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING) //debemos verificar la validez d los cells, y si realmente el cellquese conecta tiene ese #
    protected String phone;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, index = true)
    protected String description;
    @DatabaseField(canBeNull = false, dataType = DataType.STRING, index = true)
    protected String photo; @DatabaseField(canBeNull = false, dataType = DataType.STRING, uniqueIndex = true)
    protected String username;

    @ForeignCollectionField(eager = false, orderColumnName = "dateTime", orderAscending = false, foreignFieldName = "user_receiver")
    private ForeignCollection<Message> messages_receiver; //1-N

    @ForeignCollectionField(eager = false, orderColumnName = "dateTime", orderAscending = false, foreignFieldName = "user_emisor")
    private ForeignCollection<Message> messages_emisor; //1-N

    public User() {
    }

    public User(String name, String lastName, String phone, String email) {
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ForeignCollection<Message> getMessages_receiver() {
        return messages_receiver;
    }

    public void setMessages_receiver(ForeignCollection<Message> messages_receiver) {
        this.messages_receiver = messages_receiver;
    }

    public ForeignCollection<Message> getMessages_emisor() {
        return messages_emisor;
    }

    public void setMessages_emisor(ForeignCollection<Message> messages_emisor) {
        this.messages_emisor = messages_emisor;
    }
}
