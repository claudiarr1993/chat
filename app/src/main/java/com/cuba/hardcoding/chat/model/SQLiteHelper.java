package com.cuba.hardcoding.chat.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.cuba.hardcoding.chat.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

public class SQLiteHelper extends OrmLiteSqliteOpenHelper {
    private static SQLiteHelper sInstance; //singleton
    // Database Info
    private static final String DATABASE_NAME = "Chats.db";
    private static final int DATABASE_VERSION = 2;
    private final Context context;

    // DAO's (revisar bien, pq tal vez deba poner en las Entities: Integer en vez de "int")
    private Dao<User, Integer> userDao;
    private Dao<Message, Integer> messageDao;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Message.class);

            getUserDao();
            User user = new User("Pepe", "Fermín Ruiz", "+53 55896700", "ppfr@mail.com");
            if (createUser(user) == 0) {
                Toast.makeText(context, "No se pudo crear el usuario.", Toast.LENGTH_LONG).show();
            }

        } catch (SQLException e) {
            Log.e(SQLiteHelper.class.getName(), "Unable to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int currentVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Message.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(SQLiteHelper.class.getName(), "Unable to upgrade database from version " + currentVersion + " to new "
                    + newVersion, e);
        }
    }

    public static synchronized SQLiteHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new SQLiteHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    //get DAO's
    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
            // enable least-recently-used cache with max 100 items per class
//            customUserDao.setObjectCache(new LruObjectCache(10));
        }
        return userDao;
    }

    public Dao<Message, Integer> getMessageDao() throws SQLException {
        if (messageDao == null) {
            messageDao = getDao(Message.class);
            // enable least-recently-used cache with max 100 items per class
//            placeDao.setObjectCache(new LruObjectCache(100));
        }
        return messageDao;
    }

    //CRUD OPERATIONS OVER USER
    public int createUser(final User user) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<Integer>() {
                    public Integer call() throws Exception {
                        // insert our order
                        return userDao.create(user);
                        // you could pass back an object here
//                        return null;
                    }
                });
    }

    public Dao.CreateOrUpdateStatus createOrUpdateUser(final User user) throws SQLException {
        // we need the final to see it within the Callable
//        final Place p = place;
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<Dao.CreateOrUpdateStatus>() {
                    public Dao.CreateOrUpdateStatus call() throws Exception {
                        // insert our order
                        return userDao.createOrUpdate(user);
                        // you could pass back an object here
//                        return null;
                    }
                });
    }

    public int updateUser(final User user) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<Integer>() {
                    public Integer call() throws Exception {
                        return userDao.update(user);
                    }
                });
    }

    public int deleteUser(final User user) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<Integer>() {
                    public Integer call() throws Exception {
                        // insert our order
                        return userDao.delete(user);
                        // you could pass back an object here
//                        return null;
                    }
                });
    }

    //to watch user's info
    public User readUser(final int id) throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<User>() {
                    public User call() throws Exception {
                        // insert our order
                        return userDao.queryForId(id);
                        // you could pass back an object here
                    }
                });
    }

    public List<User> readAllUser() throws SQLException {
        return TransactionManager.callInTransaction(connectionSource,
                new Callable<List<User>>() {
                    public List<User> call() throws Exception {
                        // insert our order
                        return userDao.queryForAll();
                        // you could pass back an object here
//                        return null;
                    }
                });
    }

}
