package com.cuba.hardcoding.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.cuba.hardcoding.chat.model.SQLiteHelper;
import com.cuba.hardcoding.chat.model.User;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

public class SettingsActivity extends AppCompatActivity {

    private SQLiteHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        dbHelper = OpenHelperManager.getHelper(this, SQLiteHelper.class);
        User user_info = null;

        try {
            dbHelper.getUserDao();
            user_info = dbHelper.readUser(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TextView name_lastName = findViewById(R.id.profile_name);

        if (user_info != null) {
            name_lastName.setText(user_info.getName() + " " + user_info.getLastName());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null){
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }
}
